package acmebank.accountmanager;

import acmebank.accountmanager.config.Reason;
import acmebank.accountmanager.config.Status;
import acmebank.accountmanager.dao.Transaction;
import acmebank.accountmanager.dto.AccountBalance;
import acmebank.accountmanager.dto.TransferBetweenAccounts;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AccountControllerTest extends AbstractTest {

    @Before
    public void setup(){
        super.setUp();
    }

    @Test
    public void getAccountBalanceForAccountOne() throws Exception {
        String url = "/account/balance";

        AccountBalance ab = new AccountBalance();
        ab.setAccountNumber("87654321");

        String inputJson = super.mapToJson(ab);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson))
                .andReturn();

        AccountBalance verify = super.mapFromJson(mvcResult.getResponse().getContentAsString(), AccountBalance.class);

        assertEquals(new BigDecimal("1000000.00"), verify.getBalance());
        assertEquals("HKD", verify.getCurrency());
        assertEquals("87654321", verify.getAccountNumber());
        assertEquals("Success", verify.getStatus());
    }

    @Test
    public void getAccountBalanceForAccountTwo() throws Exception {
        String url = "/account/balance";

        AccountBalance ab = new AccountBalance();
        ab.setAccountNumber("11111111");

        String inputJson = super.mapToJson(ab);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputJson))
                        .andReturn();

        AccountBalance verify = super.mapFromJson(mvcResult.getResponse().getContentAsString(), AccountBalance.class);

        assertEquals(new BigDecimal("1000000.00"), verify.getBalance());
        assertEquals("HKD", verify.getCurrency());
        assertEquals("11111111", verify.getAccountNumber());
        assertEquals("Success", verify.getStatus());
    }

    @Test
    public void testSuccessfulTransferBetweenTwoAccounts() throws Exception {
        String url = "/account/transfer";

        TransferBetweenAccounts tba = new TransferBetweenAccounts();
        tba.setAmount(new BigDecimal("10000"));
        tba.setCurrency("HKD");
        tba.setSenderAccountNumber("11111111");
        tba.setReceiverAccountNumber("87654321");

        String inputJson = super.mapToJson(tba);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputJson))
                        .andReturn();

        Transaction transaction = super.mapFromJson(mvcResult.getResponse().getContentAsString(), Transaction.class);

        assertEquals(tba.getSenderAccountNumber(), transaction.getSenderAccountNumber());
        assertEquals(tba.getReceiverAccountNumber(), transaction.getReceiverAccountNumber());
        assertEquals(tba.getAmount(), transaction.getAmount());
        assertEquals(Status.SUCCESS.getValue(), transaction.getStatus());
        assertEquals(tba.getCurrency(), transaction.getCurrency());
        assertEquals(Reason.COMPLETED, transaction.getReason());
        assertNotNull(transaction.getTs());
        assertNotNull(transaction.getId());
    }

    @Test
    public void testInsufficientAmountErrorTransfer() throws Exception {
        String url = "/account/transfer";

        TransferBetweenAccounts tba = new TransferBetweenAccounts();
        tba.setAmount(new BigDecimal("10000000"));
        tba.setCurrency("HKD");
        tba.setSenderAccountNumber("11111111");
        tba.setReceiverAccountNumber("87654321");

        String inputJson = super.mapToJson(tba);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputJson))
                .andReturn();

        Transaction transaction = super.mapFromJson(mvcResult.getResponse().getContentAsString(), Transaction.class);

        assertEquals(tba.getSenderAccountNumber(), transaction.getSenderAccountNumber());
        assertEquals(tba.getReceiverAccountNumber(), transaction.getReceiverAccountNumber());
        assertEquals(tba.getAmount(), transaction.getAmount());
        assertEquals(Status.INSUFFICIENT_AMOUNT.getValue(), transaction.getStatus());
        assertEquals(tba.getCurrency(), transaction.getCurrency());
        assertEquals(Reason.INSUFFICIENT_BALANCE, transaction.getReason());
        assertNotNull(transaction.getTs());
        assertNotNull(transaction.getId());
    }

    @Test
    public void testInvalidAccountIdTransfer() throws Exception {
        String url = "/account/transfer";

        TransferBetweenAccounts tba = new TransferBetweenAccounts();
        tba.setAmount(new BigDecimal("100000"));
        tba.setCurrency("HKD");
        tba.setSenderAccountNumber("22222222");
        tba.setReceiverAccountNumber("87654321");

        String inputJson = super.mapToJson(tba);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputJson))
                .andReturn();

        Transaction transaction = super.mapFromJson(mvcResult.getResponse().getContentAsString(), Transaction.class);

        assertEquals(tba.getSenderAccountNumber(), transaction.getSenderAccountNumber());
        assertEquals(tba.getReceiverAccountNumber(), transaction.getReceiverAccountNumber());
        assertEquals(tba.getAmount(), transaction.getAmount());
        assertEquals(Status.FAIL.getValue(), transaction.getStatus());
        assertEquals(tba.getCurrency(), transaction.getCurrency());
        assertEquals(Reason.ACCOUNTS_NOT_VALID, transaction.getReason());
        assertNotNull(transaction.getTs());
        assertNotNull(transaction.getId());
    }
}
