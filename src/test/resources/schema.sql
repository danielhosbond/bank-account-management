CREATE TABLE IF NOT EXISTS accounts (
    id INT AUTO_INCREMENT PRIMARY KEY,
    account_number VARCHAR(250) NOT NULL UNIQUE,
    balance NUMERIC(15,2),
    currency VARCHAR(250)
);

CREATE TABLE IF NOT EXISTS transactions (
    id INT AUTO_INCREMENT PRIMARY KEY,
    sender_account_number VARCHAR(250) NOT NULL,
    receiver_account_number VARCHAR(250) NOT NULL,
    amount NUMERIC(15,2),
    status VARCHAR(250),
    reason VARCHAR(250),
    currency VARCHAR(250),
    ts TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);