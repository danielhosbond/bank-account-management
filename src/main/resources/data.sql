--INSERT into accounts (account_number, balance, currency) VALUES
--    ('12345678', 1000000.00, 'HKD'),
--    ('88888888', 1000000.00, 'HKD');

INSERT INTO accounts (account_number, balance, currency)
    SELECT '12345678', '1000000.00', 'HKD' FROM DUAL
WHERE NOT EXISTS
    (SELECT account_number FROM accounts where account_number='12345678');

INSERT INTO accounts (account_number, balance, currency)
    SELECT '88888888', '1000000.00', 'HKD' FROM DUAL
WHERE NOT EXISTS
    (SELECT account_number FROM accounts where account_number='88888888');