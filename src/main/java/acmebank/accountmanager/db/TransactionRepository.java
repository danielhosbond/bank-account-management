package acmebank.accountmanager.db;

import acmebank.accountmanager.dao.Transaction;
import org.springframework.data.repository.CrudRepository;

public interface TransactionRepository extends CrudRepository<Transaction, Integer> {


}
