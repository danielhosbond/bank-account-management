package acmebank.accountmanager.db;

import acmebank.accountmanager.dao.Account;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

public interface AccountRepository extends CrudRepository<Account, Integer> {

    @Query(value = "SELECT * FROM accounts WHERE account_number = ?1", nativeQuery = true)
    Account findByAccountNumber(String accountNumber);

    @Modifying
    @Transactional
    @Query(value = "UPDATE accounts SET balance=(balance - ?2) WHERE account_number = ?1", nativeQuery = true)
    int subtractAmount(String accountNumber, BigDecimal amount);

    @Modifying
    @Transactional
    @Query(value = "UPDATE accounts SET balance=(balance + ?2) WHERE account_number = ?1", nativeQuery = true)
    int addAmount(String accountNumber, BigDecimal amount);
}

