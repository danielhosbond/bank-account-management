package acmebank.accountmanager.controller;

import acmebank.accountmanager.config.Reason;
import acmebank.accountmanager.config.Status;
import acmebank.accountmanager.dao.Account;
import acmebank.accountmanager.dto.AccountBalance;
import acmebank.accountmanager.dao.Transaction;
import acmebank.accountmanager.dto.TransferBetweenAccounts;
import acmebank.accountmanager.service.AccountService;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@RestController
@RequestMapping("account")
public class AccountController {

    @Autowired
    AccountService accountService;

    private static Logger logger = LoggerFactory.getLogger(AccountController.class);

    @GetMapping(value = "/balance", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AccountBalance getAccountBalance(@RequestBody AccountBalance accountBalance) {
        try {
            Account account = accountService.getAccount(accountBalance.getAccountNumber());
            accountBalance.setCurrency(account.getCurrency());
            accountBalance.setBalance(account.getBalance());
            accountBalance.setStatus("Success");
            logger.info("Balance request for account number {} served.", accountBalance.getAccountNumber());
        } catch (Exception e) {
            logger.error("Error serving balance request" ,e);
            accountBalance.setStatus("Error serving balance request.");
        }
        return accountBalance;
    }

    @PostMapping(value = "/transfer", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Transaction transfer(@RequestBody TransferBetweenAccounts transferBetweenAccounts) {
        Transaction transaction = createTransaction(transferBetweenAccounts);

        try {
            Account senderAccount = accountService.getAccount(transferBetweenAccounts.getSenderAccountNumber());
            Account receiverAccount = accountService.getAccount(transferBetweenAccounts.getReceiverAccountNumber());

            // Check accounts are valid and sender has sufficient balance
            if (ObjectUtils.allNotNull(senderAccount, receiverAccount)) {
                if (senderAccount.getBalance().compareTo(transferBetweenAccounts.getAmount()) >= 0) {
                    int subtractStatus = accountService.subtractAmount(transferBetweenAccounts.getSenderAccountNumber(), transferBetweenAccounts.getAmount());
                    int addStatus = accountService.addAmount(transferBetweenAccounts.getReceiverAccountNumber(), transferBetweenAccounts.getAmount());
                    logger.info("Subtract status {} add status {}", subtractStatus, addStatus);
                    if (subtractStatus > 0 && addStatus > 0) {
                        transaction.setStatus(Status.SUCCESS.getValue());
                        transaction.setReason(Reason.COMPLETED);
                    }
                } else {
                    transaction.setStatus(Status.INSUFFICIENT_AMOUNT.getValue());
                    transaction.setReason(Reason.INSUFFICIENT_BALANCE);
                }
            } else {
                transaction.setStatus(Status.FAIL.getValue());
                transaction.setReason(Reason.ACCOUNTS_NOT_VALID);
            }
            transaction.setTs(Timestamp.valueOf(LocalDateTime.now()));
            transaction = accountService.saveTransaction(transaction);
        } catch (Exception e) {
            logger.error("Error in transfer", e);
        }
        return transaction;
    }

    @GetMapping(value = "/transactions", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Iterable<Transaction> transactions() {
        try {
            return accountService.getAllTransactions();
        } catch (Exception e) {
            return null;
        }
    }

    private static Transaction createTransaction(TransferBetweenAccounts tba) {
        Transaction transaction = new Transaction();
        transaction.setSenderAccountNumber(tba.getSenderAccountNumber());
        transaction.setReceiverAccountNumber(tba.getReceiverAccountNumber());
        transaction.setAmount(tba.getAmount());
        transaction.setCurrency("HKD"); // only currency supported at this time.
        return transaction;
    }
}
