package acmebank.accountmanager.config;

public enum Status {
    SUCCESS("SUCCESS"),
    FAIL("FAIL"),
    INSUFFICIENT_AMOUNT("INSUFFICIENT_AMOUNT");

    private String value;

    public String getValue(){
        return value;
    }
    private Status(String value) {
        this.value = value;
    }
}
