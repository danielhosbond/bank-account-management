package acmebank.accountmanager.config;

public class Reason {
    public static String COMPLETED = "Transaction completed.";
    public static String INSUFFICIENT_BALANCE = "Sender account has insufficient balance.";
    public static String ACCOUNTS_NOT_VALID = "Accounts are not valid.";
}
