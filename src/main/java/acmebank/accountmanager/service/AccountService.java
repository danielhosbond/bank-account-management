package acmebank.accountmanager.service;

import acmebank.accountmanager.db.AccountRepository;
import acmebank.accountmanager.db.TransactionRepository;
import acmebank.accountmanager.dao.Account;
import acmebank.accountmanager.dao.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    public Account getAccount(String accountNumber) {
        return accountRepository.findByAccountNumber(accountNumber);
    }

    public int addAmount(String accountNumber, BigDecimal amount) {
        return accountRepository.addAmount(accountNumber, amount);
    }

    public int subtractAmount(String accountNumber, BigDecimal amount) {
        return accountRepository.subtractAmount(accountNumber, amount);
    }

    public Transaction saveTransaction(Transaction transaction){
        return transactionRepository.save(transaction);
    }

    public Iterable<Transaction> getAllTransactions() {
        return transactionRepository.findAll();
    }
}
