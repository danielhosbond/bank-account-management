# Bank account manager demo

## How to install
Application requires maven and Java 11.

Build and run tests
```
mvn clean install
```

Run the jar after install
```
java -jar /target/account-manager-0.0.1-SNAPSHOT.jar
```

## Endpoints and example requests

### GET account balance
I've provided curl commands for testing, tools like Insomnia and Postman can also be used.

curl command
```
curl -X GET \
  -H "Content-type: application/json" \
  -H "Accept: application/json" \
  -d '{"accountNumber":"12345678"}' \
  "http://localhost:8080/account/balance"
```
Expected response
```
{
  "accountNumber": "12345678",
  "balance": 1090000.00,
  "currency": "HKD",
  "status": "Success"
}
```

### POST Transfer between two accounts
curl command
```
curl -X POST \
  -H "Content-type: application/json" \
  -H "Accept: application/json" \
  -d '{"senderAccountNumber":"12345678", "receiverAccountNumber": "88888888", "amount":"10000"}' \
  "http://localhost:8080/account/transfer"
```
Expected output
```
{
  "id": 12,
  "senderAccountNumber": "12345678",
  "receiverAccountNumber": "88888888",
  "amount": 10000,
  "status": "SUCCESS",
  "reason": "Transaction completed.",
  "currency": HKD,
  "ts": "2021-08-17T09:11:26.138+00:00"
}
```

### GET transactions
Each and every transaction is saved to DB, here's a command to retrieve all transactions.
```
curl -X GET \
  -H "Content-type: application/json" \
  -H "Accept: application/json" \
  "http://localhost:8080/account/transactions"
```
Example of output
```
...
 {
    "id": 6,
    "senderAccountNumber": "88888888",
    "receiverAccountNumber": "88888888",
    "amount": 30000.00,
    "status": "SUCCESS",
    "reason": "Transaction completed.",
    "currency": "HKD",
    "ts": "2021-08-17T07:04:05.355+00:00"
  },
  {
    "id": 7,
    "senderAccountNumber": "12345678",
    "receiverAccountNumber": "88888888",
    "amount": 30000000.00,
    "status": "INSUFFICIENT_AMOUNT",
    "reason": "Sender account has insufficient balance.",
    "currency": "HKD",
    "ts": "2021-08-17T07:04:31.507+00:00"
  },
  {
    "id": 8,
    "senderAccountNumber": "12345678",
    "receiverAccountNumber": "888888",
    "amount": 30000000.00,
    "status": "FAIL",
    "reason": "Accounts are not valid.",
    "currency": "HKD",
    "ts": "2021-08-17T07:04:48.511+00:00"
  },
  ...
```